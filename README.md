# Undefine.dev website

Undefine.dev is my personal website/portfolio, consisting of a remix frontend and a strapi cms.

## Installation

```bash
yarn
yarn dev
```

copy .env.dist as .env in remix and strapi  
create api key in strapi, copy that to remix .env

## Roadmap

Planned:

- Handle errors
- Portfolio of projects
- Add testing
- Autoseed database with example profile data
- Fix mobile view

Done:

- ~~Create application specific overrides for profile~~
- ~~Add linter and fix code style~~
- ~~Add German layout and locale~~
- ~~Add cover letter generation~~

Postponed/retracted:

- Switch to GraphQL for strapi data fetching
